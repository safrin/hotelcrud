<%@include file="taglib_includes.jsp" %>

<html>
    <head>
        <script src="<c:url value='/resources/js/func.js' />" type="text/javascript"></script>
        <title>Hotel Management</title>
    </head>
    <body style="font-family: Arial; font-size:smaller;background: activecaption;">

    <center>
        <div style="padding-top: 10%;">
            <h1>Hotel Management</h1>
            <form:form action="saveHotel.do" commandName="newHotel" method="post">
                <table style="border-collapse: collapse; width: 500px;background: lightblue;" cellpadding="2" cellspacing="2">
                    <tbody>
                        <tr>       
                            <td align="right" width="100">Name</td>  
                            <td width="150"><form:input path="name"></form:input></td> 

                            </tr>

                            <tr>       
                                <td align="right" width="100">Address</td> 
                                <td><form:input path="address"></form:input></td>                                      
                            </tr>
                            <tr>   
                                <td align="right" width="100">Location</td>    
                                <td><form:input path="location"></form:input></td>  
                            </tr>

                            <tr>       
                                <td colspan="2" align="center">
                                    <input name="" value="Save" type="submit">
                                    <input name="" value="Reset" type="reset">                          
                                    <input onclick="javascript:go('viewAllHotels.do');" value="Back" type="button">
                                </td> 
                            </tr>
                        </tbody>
                    </table>
            </form:form>
        </div>
    </center>
</html>
