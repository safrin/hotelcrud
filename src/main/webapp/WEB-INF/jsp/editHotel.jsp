<%@include file="taglib_includes.jsp" %>

<html>
    <head>        
        <script src="<c:url value='/resources/js/func.js' />" type="text/javascript"></script>

        <title>Hotel Management</title>
    </head>
    <body style="font-family: Arial; font-size:smaller;background: activecaption;">

    <center>
        <div style="padding-top: 10%;">
            <h1>Hotel Management</h1>
            <form:form action="updateHotel.do" commandName="editHotel" method="post">
                <table style="border-collapse: collapse; width: 500px;background: lightblue;" cellpadding="2" cellspacing="2">
                    <tbody>
                        <tr> 
                            <td align="right" width="100">Id</td>      
                            <td width="150"><form:input path="id" readonly="true"></form:input></td>                                       
                            </tr>
                            <tr>      
                                <td align="right" width="100">Name</td> 
                                <td><form:input path="name"></form:input></td>                                          
                            </tr>

                            <tr>      
                                <td align="right" width="100">Address</td> 
                                <td><form:input path="address"></form:input></td>   

                            </tr>
                            <tr>     
                                <td align="right" width="100">Location</td>  
                                <td><form:input path="location"></form:input></td>  
                            </tr>

                            <tr valign="bottom">     
                                <td colspan="3" align="center">
                                    <input onclick="javascript:deleteHotel('deleteHotel.do?id=${editHotel.id}');" value="Delete" type="button">
                                <input name="" value="Save" type="submit">
                                <input onclick="javascript:go('viewAllHotels.do');" value="Back" type="button"></td>    
                        </tr>
                    </tbody>
                </table>
            </form:form>
        </div>
    </center>

</body>
</html>
