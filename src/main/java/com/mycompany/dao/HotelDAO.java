/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dao;

import com.mycompany.entity.Hotel;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author safrin
 */
@Repository
@Transactional
public class HotelDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public Hotel getById(int id) {
        return (Hotel) sessionFactory.getCurrentSession().get(Hotel.class, id);
    }

    @SuppressWarnings("unchecked")
    public List<Hotel> search(String location) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Hotel.class);
        criteria.add(Restrictions.ilike("location", location + "%"));
        criteria.addOrder(Order.asc("location"));
        criteria.addOrder(Order.asc("name"));
        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    public List<Hotel> getAll() {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Hotel.class);
        return criteria.list();
    }

    public int save(Hotel contact) {
        return (Integer) sessionFactory.getCurrentSession().save(contact);
    }

    public void update(Hotel contact) {
        sessionFactory.getCurrentSession().merge(contact);
    }

    public void delete(int id) {
        Hotel c = getById(id);
        sessionFactory.getCurrentSession().delete(c);
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

}
